/**
 * 
 */

(function() {
  'use strict';

  angular
    .module('app', [
      'auth0',
      'angular-jwt',
      'app.common',
      'app.controllers',
      'app.services'
    ])
    .config(config)
    .run(run);

  config.$inject = ['$qProvider', '$stateProvider', '$locationProvider', '$urlRouterProvider', 'authProvider', 'AUTH0_DOMAIN', 'AUTH0_CLIENT_ID'];

  function config($qProvider, $stateProvider, $locationProvider, $urlRouterProvider, authProvider, AUTH0_DOMAIN, AUTH0_CLIENT_ID) {

    // Hide 'Possibly unhandled rejection: {}' angular bug
    $qProvider.errorOnUnhandledRejections(false);

    authProvider.init({
      domain: AUTH0_DOMAIN,
      clientID: AUTH0_CLIENT_ID
    })

    $urlRouterProvider.otherwise('/');

    $locationProvider.hashPrefix('');
  }

  run.$inject = ['$transitions', '$rootScope', '$state', '$stateParams', 'jwtHelper'];

  function run($transitions, $rootScope, $state, $stateParams, jwtHelper) {

    // Disable the default error handler in production mode
    $state.defaultErrorHandler(function() { /* do nothing */ });

    // Send the user to the find state when no url is present.
    // This only happens when the user is logged in and he removes '/#/'
    // from the url.
    $transitions.onSuccess({}, function($transitions) {
      let newToState = $transitions.$to();
      if (newToState.self.url === "") {
        $state.go('find');
      }
    })

    // When 'auth' is present in the state name and the user is NOT logged in
    // we'll redirect him to the find state.
    $transitions.onStart({ to: 'auth.**' }, function(trans) {
      const token = localStorage.getItem('id_token');
      if (token && !jwtHelper.isTokenExpired(token)) {
        console.log('run...we have a token and is not expired, we can continue');
        return;
      } else {
        console.log('run...no token or token is expired, we can\'t continue');
        localStorage.removeItem('name');
        localStorage.removeItem('id_token');
        $state.go('find');
        return;
      }
    })

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }

})();
