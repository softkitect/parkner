/**
 * Here we register all the services used in the application.
 */

(function() {
  'use strict';

  angular
    .module('app.services', [
      'amenity.service',
      'auth.service',
      'css-injector.factory',
      'lot.service',
      'places.service',
      'reservation.service',
      'user.service',
    ]);

})();
