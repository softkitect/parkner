/**
 * We use auth0 to authenticate the user. 
 * auth0 will respond with a user profile and a token. 
 * We use getUserId to find the user id from Parkner's API; finally we save
 * in localstorage the user's name, email, id and token.
 */

(function() {
  'use strict';

  angular
    .module('auth.service', ['angular-jwt', 'ui.router'])
    .service('AuthService', AuthService);

  AuthService.$inject = ['auth', '$state', 'jwtHelper', 'UserService'];

  function AuthService(auth, $state, jwtHelper, UserService) {

    function login() {
      // Signin the user with his username & password or social network account.
      // If the user signs in with his google or facebook account and he is not
      // registered in auth0 he will be registered with the data available in 
      // the profile returned from google or facebook
      auth.signin({}, function(profile, token) {
        console.log("**PROFILE: ", profile);
        console.log("**TOKEN: ", token);
        // If there is a token in the auth0 response...
        if (token) {
          // We'll see if the user is registered in Parkner
          UserService.getUserId(profile.email)
            .then(function(userId) {
              // If we don't found the user's email in the Parner DB 
              // we'll try to create an account for him
              if (!userId) {
                let parknerData = {};
                parknerData.first = profile.given_name;
                parknerData.last = profile.family_name;
                parknerData.email = profile.email;

                UserService.postUser(parknerData)
                  .then(function(response) {
                    setSession(profile, token, response.data.id);
                    return;
                  }, function(error) {
                    return error;
                  });
              } else {
                // We found the user and it wasn't necessary to create 
                // an account. We were able to get the userId and set it
                // in the session
                setSession(profile, token, userId);
              }
            })

          if ($state.$current.name == "successful-user-registration") {
            $state.go('find');
          }
          return;
        }
      }, function(error) {
        return error;
      });
    }

    // Save the user information in localstorage.
    function setSession(profile, token, userId) {
      localStorage.setItem('name', profile.name);
      localStorage.setItem('email', profile.email);
      localStorage.setItem('user_id', userId);
      localStorage.setItem('id_token', token);
      return;
    }

    // Clear the user information from localstorage.
    function logout() {
      localStorage.removeItem('name');
      localStorage.removeItem('email');
      localStorage.removeItem('user_id');
      localStorage.removeItem('id_token');
      auth.signout();
      $state.go('find');
    }

    // Check if a token is still valid.
    function isAuthenticated() {
      const token = localStorage.getItem('id_token');
      if (token && !jwtHelper.isTokenExpired(token)) {
        return true;
      } else {
        return false;
      }
    }

    return {
      login: login,
      logout: logout,
      isAuthenticated: isAuthenticated,
    }

  }

})();
