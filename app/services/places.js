/**
 * 
 */

(function() {
  'use strict';

  angular
    .module('places.service', [])
    .service('PlacesService', PlacesService);

  function PlacesService() {
    let service = this;

    service.getLocations = function(center) {
      let places = {};

      var location_name = center.address;
      var obj = {
        lat: center.lat,
        lng: center.lng,
        message: center.address
      };
      places[location_name] = obj;

      var location_name = "Tacna";
      var obj = {
        lat: -18.006569,
        lng: -70.246277,
        message: 'Tacna'
      };
      places[location_name] = obj;

      var location_name = "Pucallpa";
      var obj = {
        lat: -8.392862,
        lng: -74.582619,
        message: 'Pucallpa'
      };
      places[location_name] = obj;

      var location_name = "Langa";
      var obj = {
        lat: -12.125033,
        lng: -76.421181,
        message: 'Langa'
      };
      places[location_name] = obj;

      var location_name = "Huaraz";
      var obj = {
        lat: -9.526115,
        lng: -77.528778,
        message: 'Huaraz'
      };
      places[location_name] = obj;

      var location_name = "Chiclayo";
      var obj = {
        lat: -6.7815551,
        lng: -79.8840601,
        message: 'Chiclayo'
      };
      places[location_name] = obj;

      var location_name = "Chepen";
      var obj = {
        lat: -7.2293772,
        lng: -79.4412924,
        message: 'Chepen'
      };
      places[location_name] = obj;

      var location_name = "Punta Hermosa";
      var obj = {
        lat: -12.2625884,
        lng: -76.8157737,
        message: 'Punta Hermosa'
      };
      places[location_name] = obj;

      var location_name = "Punta Negra";
      var obj = {
        lat: -12.3006809,
        lng: -76.7892077,
        message: 'Punta Negra'
      };
      places[location_name] = obj;

      var location_name = "Chaclacayo";
      var obj = {
        lat: -11.9889455,
        lng: -76.8023483,
        message: 'Chaclacayo'
      };
      places[location_name] = obj;

      var location_name = "Cieneguilla";
      var obj = {
        lat: -12.0700974,
        lng: -76.8349453,
        message: 'Cieneguilla'
      };
      places[location_name] = obj;

      var location_name = "Pisco";
      var obj = {
        lat: -13.7115708,
        lng: -76.2222091,
        message: 'Pisco'
      };
      places[location_name] = obj;

      var location_name = "Paracas";
      var obj = {
        lat: -13.8348491,
        lng: -76.2674038,
        message: 'Paracas'
      };
      places[location_name] = obj;

      return places;
      /*return $http
          .get(API_BASE_PATH + '/lots')
          .then(function(response) {
              return response;
          }, function(error) {
              return error;
          });*/
    };
  }

})();
