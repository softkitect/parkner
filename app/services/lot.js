/**
 * This service is used to do http requests to the lot endpoint.
 */

(function() {
  'use strict';

  angular
    .module('lot.service', [])
    .service('LotService', LotService);

  LotService.$inject = ['$http', 'CLOUDINARY_UPLOAD_PATH', 'PARKNER_API_BASE_PATH'];

  function LotService($http, CLOUDINARY_UPLOAD_PATH, PARKNER_API_BASE_PATH) {
    let service = this;

    //                                                                      
    // /lots endpoint                                                       
    //                                                                      

    // GET /lots
    service.getLots = function() {
      const URL = PARKNER_API_BASE_PATH + '/lots';
      return $http
        .get(URL)
        .then(function(response) {
          return response.data;
        })
        .catch(function(e) {
          alert('Hubo un error al procesar la información. Por favor intente más tarde.');
          return;
        })
        .finally(function() {
          // console.log('This finally block');
        });
    };

    // GET /lots/{userId}
    service.getMyLots = function(userId) {
      const URL = PARKNER_API_BASE_PATH + '/lots' + '/' + userId;
      return $http
        .get(URL)
        .then(function(response) {
          let lots = [];
          let objects = response.data;
          if (objects.length !== 0) {
            for (let obj of objects) {
              let lot = {};

              let id = obj.id;
              let address = obj.address;
              let cost = obj.cost;
              let startAvailability = obj.startAvailability;
              let endAvailability = obj.endAvailability;
              let active = obj.active;

              lot = {
                id: id,
                description: address.description,
                cost: cost,
                startAvailability: startAvailability,
                endAvailability: endAvailability,
                active: active,
              };

              lots.push(lot);
            }
          }
          return lots;
        }, function(error) {
          return error;
        });
    };

    // POST /lots/{id}
    service.postLot = function(userId, lot) {
      const URL = PARKNER_API_BASE_PATH + '/lots' + '/' + userId;
      return $http
        .post(URL, lot)
        .then(function(response) {
          return response;
        }, function(error) {
          return error;
        });
    };

    //                                                                      
    // Upload image to Cloudinary                                           
    //                                                                      

    service.uploadFile = function(image) {
      return $http
        .post(CLOUDINARY_UPLOAD_PATH, image, {
          headers: {
            'Content-Type': undefined,
            'X-Requested-With': 'XMLHttpRequest'
          }
        })
        .then(function(response) {
          let imageUrl = response.data.url;
          return imageUrl;
        }, function(error) {
          return error;
        });
    };

  }

})();
