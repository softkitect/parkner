/**
 * This service is used to add a specific css to a view.
 * Check the note bellow from removeLastLink.
 */

(function() {
  angular
    .module('css-injector.factory', ['ui.router'])
    .factory("CSSInjector", CSSInjector);

  CSSInjector.$inject = ['$q', '$http'];

  function CSSInjector($q, $http) {
    var service = {};

    var createLink = function(id, cssFile) {

      var CSS_PATH = "app/css/";
      var link = document.createElement('link');
      link.id = id;
      link.rel = "stylesheet";
      link.type = "text/css";
      link.href = CSS_PATH + cssFile;
      return link;
    }

    var checkLoaded = function(cssFile, deferred, tries) {
      for (var i in document.styleSheets) {
        var href = document.styleSheets[i].href || "";
        if (href.split("/").slice(-1).join() === cssFile) {
          deferred.resolve();
          return;
        }
      }
      tries++;
      setTimeout(function() { checkLoaded(cssFile, deferred, tries); }, 50);
    };

    var removeLastLink = function(cssFile) {
      // Any other css file that isn't included in this array will be 
      // deleted from the DOM
      var fixedStyles = ["base.css", "mini.css", "dropzone.css", "jquery-ui.css", "leaflet.css", "ng-modal.css"];
      fixedStyles.push(cssFile);

      // Loop through all the styleSheets in the DOM
      for (var i in document.styleSheets) {
        var href = document.styleSheets[i].href || "";
        if (href == "") { continue; }

        // Get the href file name
        var hrefName = href.split("/").slice(-1).join();

        // Remove the element from the DOM if the css file isn't inside
        // fixedStyles
        if (fixedStyles.indexOf(hrefName) === -1) {
          var id = hrefName.slice(0, -4);
          var elem = document.getElementById(id);
          elem.parentNode.removeChild(elem);
        }
      }
      return;
    }

    service.set = function(id, cssFile) {
      removeLastLink(cssFile);

      var tries = 0,
        deferred = $q.defer(),
        link;

      if (!angular.element('link#' + id).length) {
        link = createLink(id, cssFile);
        link.onload = deferred.resolve;
        angular.element('head').append(link);
      }
      checkLoaded(cssFile, deferred, tries);

      return deferred.promise;
    };

    return service;
  }

})();
