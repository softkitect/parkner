/**
 * Holds constants to be used in the entire application.
 */

(function() {
  'use strict';

  angular
    .module('app.common', [])
    .constant('API_BASE_PATH', "https://httpbin.org/")
    .constant('AUTH0_CLIENT_ID', "eT32q7IEdnkJYz63DgHlLmsZ0VJ5cTla")
    .constant('AUTH0_CONNECTION', "Username-Password-Authentication")
    .constant('AUTH0_DOMAIN', "parkner-dev.auth0.com")
    .constant('AUTH0_SIGNUP_ENDPOINT', "https://softkitect.auth0.com/dbconnections/signup")
    .constant('CLOUDINARY_UPLOAD_PATH', "https://api.cloudinary.com/v1_1/softkitect/image/upload/")
    .constant('CLOUDINARY_UPLOAD_PRESET', "u8e4fjza")
    .constant('PARKNER_API_BASE_PATH', "https://parknerapidev.azurewebsites.net/api")
    // Status codes
    .constant('STATUS_CODE_DELETE', 200)
    .constant('STATUS_CODE_GET', 200)
    .constant('STATUS_CODE_CREATED', 201)
    .constant('STATUS_CODE_PUT', 200)
    .constant('STATUS_CODE_NO_CONTENT', 204)
    .config(config);

  config.$inject = ['$httpProvider'];

  function config($httpProvider) {
    $httpProvider.interceptors.push('LoadingHttpInterceptor');
  }

})();