/**
 * Component used to display a loading gif on every http request.
 */

(function() {
  'use strict';

  angular
    .module('app.common')
    .component('loading', {
      template: '<img src="app/img/loading.svg" ng-if="$ctrl.show">',
      controller: LoadingController
    });

  LoadingController.$inject = ['$rootScope'];

  function LoadingController($rootScope) {
    var $ctrl = this;
    let listener;

    $ctrl.$onInit = function() {
      $ctrl.show = false;
      listener = $rootScope.$on('spinner:activate', onSpinnerActivate);
    };

    $ctrl.$onDestroy = function() {
      listener();
    };

    function onSpinnerActivate(event, data) {
      $ctrl.show = data.on;
    }
  }

})();
