/**
 * It shows a success message when a new vehicle has been registered.
 */

(function() {
  'use strict';

  angular
    .module('successful-vehicle-registration.controller', [])
    .config(function($stateProvider) {
      $stateProvider
        .state('successful-vehicle-registration', {
          url: '/successful-vehicle-registration',
          templateUrl: 'app/views/successful-vehicle-registration.html',
          controller: 'SuccessfulVehicleRegistrationController as vm',
          data: {
            pageTitle: 'Registro Exitoso',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("successful-vehicle-registration", "successful-vehicle-registration.css");
            }]
          }
        });
    })
    .controller('SuccessfulVehicleRegistrationController', SuccessfulVehicleRegistrationController);

  SuccessfulVehicleRegistrationController.$inject = [];

  function SuccessfulVehicleRegistrationController() {}

})();
