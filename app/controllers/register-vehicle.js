/**
 * Main function is to register a new vehicle.
 */

(function() {
  'use strict';

  angular
    .module('register-vehicle.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.register-vehicle', {
          url: '/register-vehicle',
          templateUrl: 'app/views/register-vehicle.html',
          controller: 'RegisterVehicleController as vm',
          data: {
            pageTitle: 'Registrar Vehículo',
            background: 'cityImage'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("register-vehicle", "register-vehicle.css");
            }]
          }
        });
    })
    .controller('RegisterVehicleController', RegisterVehicleController);

  RegisterVehicleController.$inject = ['UserService', '$state', 'STATUS_CODE_CREATED'];

  function RegisterVehicleController(UserService, $state, STATUS_CODE_CREATED) {
    let vm = this;
    const userId = localStorage.getItem('user_id');
    let created = false;

    vm.register = function(form) {
      let vehicle = {};
      vehicle.make = form.make;
      vehicle.model = form.model;
      vehicle.plate = form.plate;
      vehicle.color = form.color;

      UserService
        .postUserCar(userId, form)
        .then(function(response) {
          if (response.status !== STATUS_CODE_CREATED) {
            let data = response.data;
            alert(data.description);
            return;
          }
          $state.go('successful-vehicle-registration');
        });

    }
  }

})();
