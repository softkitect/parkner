/**
 * Display a list of lots. It shows ONLY the lots owned by the user that is 
 * logged in.
 */

(function() {
  'use strict';

  angular
    .module('lot-listing.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.lot-listing', {
          url: '/lot-listing',
          templateUrl: 'app/views/lot-listing.html',
          controller: 'LotListingController as vm',
          resolve: {
            lots: ['LotService', function(LotService) {
              const userId = localStorage.getItem('user_id');
              return LotService.getMyLots(userId);
            }],
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("lot-listing", "lot-listing.css");
            }]
          },
          data: {
            pageTitle: 'Mis Espacios',
            background: 'cars'
          }
        });
    })
    .controller('LotListingController', LotListingController);

  LotListingController.$inject = ['lots'];

  function LotListingController(lots) {
    let vm = this;
    vm.lots = lots;

    vm.hasLots = lots.length >= 1;
  }

})();
