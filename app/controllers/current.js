/**
 * It is used to know the status of the user.
 */

(function() {
  'use strict';

  angular
    .module('current.controller', [])
    .controller('CurrentUserController', CurrentUserController);

  CurrentUserController.$inject = ['AuthService'];

  function CurrentUserController(AuthService) {
    let vm = this;

    vm.isAuthenticated = AuthService.isAuthenticated;
    vm.login = AuthService.login;
    vm.logout = AuthService.logout;
  }

})();
