/**
 * This is the first view showed to the user.
 * We are using Places API:
 * https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete
 */

(function() {
  'use strict';

  angular
    .module('find.controller', ['ui.router', 'ngMap'])
    .config(function($stateProvider) {
      $stateProvider
        .state('find', {
          url: '/',
          templateUrl: 'app/views/find.html',
          controller: 'FindController as vm',
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("find", "find.css");
            }]
          },
          data: {
            pageTitle: 'Buscar'
          }
        });
    })
    .controller('FindController', FindController);

  FindController.$inject = ['NgMap', '$state', '$http'];

  function FindController(NgMap, $state, $http) {
    let vm = this;

    vm.placeChanged = function() {
      vm.place = this.getPlace();
      $state.go('get-map', { place: vm.place });
    };
  }

})();