/**
 * We use this controller to fetch and display all the lots from the `/lots` 
 * endpoint.
 * The map is powered by:
 * leaflet.js
 * angular-leaflet-directive
 * leaflet-plugins
 */

(function() {
  'use strict';

  angular
    .module('map.controller', ['ui.router', 'leaflet-directive', 'chart.js'])
    .config(function($stateProvider) {
      $stateProvider
        .state('get-map', {
          url: '/get-map',
          params: {
            place: null
          },
          templateUrl: 'app/views/get-map.html',
          controller: 'MapController as vm',
          data: {
            pageTitle: 'Espacios disponibles',
            background: 'cars'
          },
          resolve: {
            lots: ['LotService', function(LotService) {
              return LotService.getLots();
            }],
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("get-map", "get-map.css");
            }]
          }
        });
    })
    .controller('MapController', MapController);

  MapController.$inject = ['$stateParams', '$state', '$scope', 'PlacesService', 'leafletData', 'lots'];

  function MapController($stateParams, $state, $scope, PlacesService, leafletData, lots) {
    var vm = this;
    vm.lots = lots;

    var lat;
    var lng;
    var address;
    var places = {};

    vm.layers = {
      baselayers: {
        bingRoad: {
          name: 'Bing Road',
          type: 'bing',
          key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
          layerOptions: {
            type: 'Road'
          }
        }
      },
      events: {
        markers: {
          enable: ['click']
        }
      }
    }

    if ($state.params.place != null) {
      lat = $state.params.place.geometry.location.lat();
      lng = $state.params.place.geometry.location.lng();
      address = $state.params.place.name;
    } else {
      lat = -12.2720956;
      lng = -76.27108329999999;
      address = "Lima Region";
    }

    var center = {
      lat: lat,
      lng: lng,
      address: address
    };

    for (let lot of lots) {
      var id = lot.id;
      var lat = lot.address.location.lat;
      var lng = lot.address.location.lng;
      var location_name = lot.address.description;

      var obj = {
        lat: lat,
        lng: lng,
        message: location_name
      };
      places[id] = obj;
    }

    vm = {
      center: {
        lat: lat,
        lng: lng,
        zoom: 8
      },
      markers: places
    }

    vm.placeChanged = function() {
      vm.place = this.getPlace();
      lat = vm.place.geometry.location.lat();
      lng = vm.place.geometry.location.lng();
      address = vm.place.name;

      center = {
        lat: lat,
        lng: lng,
        address: address
      };

      places = PlacesService.getLocations(center);

      vm = {
        center: {
          lat: lat,
          lng: lng,
          zoom: 8
        },
        markers: places
      }

      leafletData.getMap()
        .then(function(map) {
          map.setView([lat, lng]);
        });
    };

    $scope.$on("leafletDirectiveMarker.click", function(event, args) {
      var leafEvent = args.leafletEvent;
      console.log('clicked: ', args.modelName);
    });

    vm.showDetails = function(lot) {
      $state.go('lot-details.lot-details-1', { lot: lot });
    };

    angular.extend(this, vm);
  }

})();
