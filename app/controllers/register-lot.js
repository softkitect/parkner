/**
 * Main function is to register a lot.
 */

(function() {
  'use strict';

  angular
    .module('register-lot.controller', ['ui.router', 'ngFileUpload', 'ngModal'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.lot-registration', {
          abstract: true,
          url: '/lot-registration',
          templateUrl: 'app/views/register-lot.html',
          controller: 'RegisterLotController as vm',
          data: {
            pageTitle: 'Registra un Espacio',
            background: ''
          }
        })
        .state('auth.lot-registration.lot-registration-1', {
          url: '/registration-step-1',
          templateUrl: 'app/views/register-lot.form-1.html',
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("register-lot-form-1", "register-lot-form-1.css");
            }]
          }
        })
        .state('auth.lot-registration.lot-registration-2', {
          url: '/registration-step-2',
          templateUrl: 'app/views/register-lot.form-2.html',
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("register-lot-form-2", "register-lot-form-2.css");
            }]
          }
        });
    })
    .controller('RegisterLotController', RegisterLotController);

  RegisterLotController.$inject = [
      '$state', 'LotService', 'UserService', 'AmenityService', 
      'CLOUDINARY_UPLOAD_PRESET', 'STATUS_CODE_CREATED', 'STATUS_CODE_NO_CONTENT'
    ];

  function RegisterLotController(
    $state, LotService, UserService, AmenityService, 
    CLOUDINARY_UPLOAD_PRESET, STATUS_CODE_CREATED, STATUS_CODE_NO_CONTENT) {

    let vm = this;
    const userId = localStorage.getItem('user_id');

    vm.dialogShown = false;
    vm.toggleModal = function() {
      vm.dialogShown = !vm.dialogShown;
    };

    let lat, lng, placeId, long;
    vm.form = {};
    vm.espacios = 0;
    vm.days = [];
    vm.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    vm.years = [];
    vm.hours = [];
    vm.minutes = [];
    vm.meridian = ["AM", "PM"];

    let pad = function(n) {
      if (n < 10) {
        n = '0' + n;
      }
      return n;
    }

    // Days
    for (let i = 1; i <= 31; i++) {
      vm.days.push(i);
    }

    // Years
    for (let i = 2017; i <= 2020; i++) {
      vm.years.push(i);
    }

    // Hours
    for (let i = 1; i <= 24; i++) {
      vm.hours.push(pad(i));
    }

    // Minutes
    for (let i = 0; i <= 59; i++) {
      vm.minutes.push(pad(i));
    }

    // Amenities
    vm.form.roof = false;
    vm.form.cam = false;
    vm.form.key = false;
    vm.form.guardian = false;
    vm.form.carWash = false;
    vm.form.access = false;

    vm.amenities = [];

    // Lets create an array with all the default images
    vm.images = [
      "http://via.placeholder.com/200x200",
      "http://via.placeholder.com/200x200",
      "http://via.placeholder.com/200x200",
      "http://via.placeholder.com/200x200"
    ];

    // We'll use 'index' to replace elements in the images array
    let index = 0;

    // When ARR_LIMIT reach 4 we'll disable the file select option
    vm.ARR_LIMIT = 0;

    vm.placeChanged = function() {
      vm.place = this.getPlace();

      lat = vm.place.geometry.location.lat();
      lng = vm.place.geometry.location.lng();
      placeId = vm.place.place_id;
      long = vm.place.formatted_address;
    };

    vm.addAmenitie = function(event, isSelected) {
      let amenitie = getAmenityObject(event.target.value);
      if (isSelected) {
        vm.amenities.push(amenitie);
      } else {
        let pos = vm.amenities.indexOf(amenitie);
        vm.amenities.splice(pos, 1);
      }
    };

    vm.upload = function(file) {
      if (!file) {
        return;
      }

      let imageData = new FormData();
      imageData.append('upload_preset', CLOUDINARY_UPLOAD_PRESET);
      imageData.append('file', file);

      LotService.uploadFile(imageData)
        .then(function(response) {
          vm.images.splice(index, 1, response);
          index++;

          vm.ARR_LIMIT++;
        }, function(error) {
          console.log('Error: ', error);
        });
    };

    vm.register = function() {

      if (!vm.form.addr) {
        alert("Form is incomplete!");
        return;
      }

      // Get the index of the selected months
      let indexMonth1 = vm.months.indexOf(vm.form.month1);
      let indexMonth2 = vm.months.indexOf(vm.form.month2);

      let hh1 = vm.form.hh1 || "01";
      let mm1 = vm.form.mm1 || "01";
      let hh2 = vm.form.hh2 || "01";
      let mm2 = vm.form.mm2 || "01";

      // Create Date object
      let startAvailability = new Date(vm.form.year1, indexMonth1, vm.form.day1, hh1, mm1);
      let endAvailability = new Date(vm.form.year2, indexMonth2, vm.form.day2, hh2, mm2);

      // The end date must be greater than the initial date
      if (!(endAvailability > startAvailability)) {
        alert('Revise las fechas. La fecha final debe ser mayor a la fecha inicial.');
        return;
      }

      // The difference between the 2 dates must be greater or equal than 1 month
      let timeDiff = Math.abs(endAvailability.getTime() - startAvailability.getTime());
      let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if (!(diffDays > 31)) {
        alert('Revise las fechas. El tiempo de disponibilidad debe ser mayor a 1 mes.');
        return;
      }

      // Create lot object
      let lotData = {};
      lotData.spaces = vm.form.spaces;
      lotData.startAvailability = startAvailability;
      lotData.endAvailability = endAvailability;
      lotData.cost = vm.form.price;
      lotData.placeId = placeId;
      lotData.rules = "rules";
      lotData.description = "description";
      lotData.images = vm.images;
      lotData.amenities = vm.amenities;

      // Check if the user has payment information 
      UserService.getUserPaymentInfo(userId)
        .then(function(hasPaymentInfo) {
          // Display a modal to register the user's payament information
          if (!hasPaymentInfo) {
            vm.dialogShown = true;
          } else {
            // If the user has payment information, save the lot
            LotService.postLot(userId, lotData)
              .then(function(response) {
                if (response.status != STATUS_CODE_CREATED) {
                  alert('La información no se pudo guardar. Por favor, intenta más tarde.');
                  return;
                }
                index = 0;
                $state.go('successful-lot-registration');
              }, function(error) {
                console.log('Error: ', error);
              });
          }
        }, function(error) {
          console.log('Error: ', error);
        });
    };

    vm.addPaymentInformation = function() {
      vm.dialogShown = false;

      let paymentInfo = {};
      paymentInfo.bankName = vm.user.bankName;
      paymentInfo.bankNumber = vm.user.bankNumber;

      UserService.putUserPaymentInfo(userId, paymentInfo)
        .then(function(response) {
          console.log('Response: ', response);
          if (response.status != STATUS_CODE_NO_CONTENT) {
            alert('La información del banco no se pudo guardar. Por favor, intenta más tarde.');
            return;
          }
          alert('La información del banco se guardó satisfactoriamente, puede continuar y registrar su espacio.');
        });
    };

    function getAmenityObject(amenityName) {
      let amenities = AmenityService.getAmenities();
      for (let amenitie in amenities) {
        if (amenityName === amenities[amenitie]["name"]) {
          return amenities[amenitie];
        }
      }

    }

  }

})();
