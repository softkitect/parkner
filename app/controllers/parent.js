/**
 * All auth's child states will require the user to be logged in.
 */

(function() {
  'use strict';

  angular
    .module('parent.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth', {
          url: '',
          abstract: false,
        })
    })
    .controller('ParentController', ParentController);

  function ParentController() {}

})();
