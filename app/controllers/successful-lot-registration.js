/**
 * It shows a success message when a new lot has been registered.
 */

(function() {
  'use strict';

  angular
    .module('successful-lot-registration.controller', [])
    .config(function($stateProvider) {
      $stateProvider
        .state('successful-lot-registration', {
          url: '/successful-lot-registration',
          templateUrl: 'app/views/successful-lot-registration.html',
          controller: 'SuccessfulLotRegistrationController as vm',
          data: {
            pageTitle: 'Registro Exitoso',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("successful-lot-registration", "successful-lot-registration.css");
            }]
          }
        });
    })
    .controller('SuccessfulLotRegistrationController', SuccessfulLotRegistrationController);

  SuccessfulLotRegistrationController.$inject = [];

  function SuccessfulLotRegistrationController() {}

})();
