/**
 * Display the user payment history.
 * Data is fixed because there's no endpoint that provides this information.
 */

(function() {
  'use strict';

  angular
    .module('payment-history.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('auth.payment-history', {
          url: '/payment-history',
          templateUrl: 'app/views/payment-history.html',
          controller: 'PaymentHistoryController as vm',
          data: {
            pageTitle: 'Historial de Pagos',
            background: 'cars'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("payment-history", "payment-history.css");
            }]
          }
        });
    })
    .controller('PaymentHistoryController', PaymentHistoryController);

  function PaymentHistoryController() {
    let vm = this;

    vm.friendName = "Ricardo";
    vm.fechaComment = "Noviembre 2017";
    vm.friendComment = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of";
    vm.misCalificaciones = "50";
  }

})();
