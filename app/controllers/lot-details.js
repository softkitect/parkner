/**
 * Display, in two separate pages, the Lot details.
 * The information printed here is being received as a parameter from `lot-listing`.
 */

(function() {
  'use strict';

  angular
    .module('lot-details.controller', ['ui.router'])
    .config(function($stateProvider) {
      $stateProvider
        .state('lot-details', {
          abstract: true,
          url: '/lot-details',
          templateUrl: 'app/views/lot-details.html',
          controller: 'LotDetailsController as vm',
          data: {
            pageTitle: 'Detalles',
            background: 'cars'
          }
        })
        .state('lot-details.lot-details-1', {
          url: '/details-step-1',
          params: {
            lot: null
          },
          templateUrl: 'app/views/lot-details-1.html',
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("lot-details-1", "lot-details-1.css");
            }]
          }
        })
        .state('lot-details.lot-details-2', {
          url: '/details-step-2',
          templateUrl: 'app/views/lot-details-2.html',
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("lot-details-2", "lot-details-2.css");
            }]
          }
        });
    })
    .controller('LotDetailsController', LotDetailsController);

  LotDetailsController.$inject = ['$state', '$stateParams', 'ReservationService', 'AuthService', 'UserService', 'STATUS_CODE_CREATED'];

  function LotDetailsController($state, $stateParams, ReservationService, AuthService, UserService, STATUS_CODE_CREATED) {
    let vm = this;

    // Get the user_id from localStorage
    const userId = localStorage.getItem('user_id');

    // From AuthService, bind isAuthenticated to vm
    vm.isAuthenticated = AuthService.isAuthenticated;

    // Angular's input type=date is not supported on all browsers
    vm.daysArray = [];
    vm.monthsArray = [];
    vm.yearsArray = [];
    vm.hoursArray = [];
    vm.minutesArray = [];
    fillArrays();

    // If no parameters are present, alert the user and send him to `find`
    if ($state.params.lot == null) {
      alert('No parameters received.');
      $state.go('find');
      return;
    }

    let lot = $state.params.lot;
    vm.lotId = lot.id || "";
    vm.address = lot.address.description || "";
    vm.cost = lot.cost || "";
    vm.startAvailability = new Date(lot.startAvailability);
    vm.endAvailability = new Date(lot.endAvailability);

    // Amenities
    let amenities = lot.amenities;

    vm.guardian = false;
    vm.cam = false;
    vm.roof = false;
    vm.carWash = false;
    vm.access = false;
    vm.control = false;

    for (let amenitie of amenities) {
      switch (amenitie.name) {
        case "Seguridad":
          vm.guardian = true;
          break;
        case "Camaras":
          vm.cam = true;
          break;
        case "Techado":
          vm.roof = true;
          break;
        case "Lavado de Auto":
          vm.carWash = true;
          break;
        case "24 Horas":
          vm.access = true;
          break;
        case "Llave":
          vm.control = true;
          break;
        default:
          console.log('Amenitie not found: ', amenitie);
      }
    }

    vm.lotFirstImage = lot.images[0];

    // if (lot.spaces[0].openTimes.length > 0) {
    //   // Check if object has the `start` property
    //   if (lot.spaces[0].openTimes[0].hasOwnProperty('start') &&
    //     lot.spaces[0].openTimes[0].hasOwnProperty('end')) {
    //     // Create a Date Object with the `start` parameter received
    //     vm.startAvailability = new Date(lot.spaces[0].openTimes[0].start);
    //     vm.endAvailability = new Date(lot.spaces[0].openTimes[0].end);
    //   }
    // }

    vm.createReservation = function() {

      // Check if we have the userId
      if (!userId) {
        alert('User ID not found. Please log in to continue.');
        $state.go('find');
        return;
      }

      // Get the index of the selected months
      let indexMonth1 = vm.monthsArray.indexOf(vm.month1);
      let indexMonth2 = vm.monthsArray.indexOf(vm.month2);

      let hh1 = vm.hh1 || "01";
      let mm1 = vm.mm1 || "01";
      let hh2 = vm.hh2 || "01";
      let mm2 = vm.mm2 || "01";

      // Set Duration Dates
      let startDate = new Date(vm.year1, indexMonth1, vm.day1, hh1, mm1);
      let endDate = new Date(vm.year2, indexMonth2, vm.day2, hh2, mm2);

      let reservationData = {};
      reservationData.lotId = vm.lotId;
      reservationData.customerId = userId;
      // TODO: Let the user select from his cars.
      reservationData.car = {
          make: "make",
          model: "model",
          plate: "plate",
          color: "color"
      };
      reservationData.startDate = startDate;
      reservationData.endDate = endDate;
      reservationData.type = "MONTHLY";
      reservationData.referralCode = "referralCode";

      ReservationService.postReservation(reservationData)
        .then(function(response) {
          const data = response.data;
          if (response.status != STATUS_CODE_CREATED) {
            const description = data.description;
            alert('Se presentó un error al realizar la reservación. Por favor verifique que la información que ingresó sea correcta.\nError: ' + description);
            return;
          }
          alert('La reservación del espacio se ha hecho.');
          $state.go('find');
          return;
        });

    }

    vm.addCard = function() {
      vm.dialogShown = false;

      let card = vm.card.token;

      UserService.postUserCard(userId, card)
        .then(function(response) {
          console.log('Response: ', response);
        }, function(error) {
          console.log('Error: ', error);
        });
    };

    function pad(n) {
      if (n < 10) {
        n = '0' + n;
      }
      return n;
    }

    function fillArrays() {
      // Days
      for (let i = 1; i <= 31; i++) {
        vm.daysArray.push(i);
      }

      // Months
      vm.monthsArray = [
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
      ];

      // Years
      for (let i = 2017; i <= 2020; i++) {
        vm.yearsArray.push(i);
      }

      // Hours
      for (let i = 1; i <= 24; i++) {
        vm.hoursArray.push(pad(i));
      }

      // Minutes
      for (let i = 0; i <= 59; i++) {
        vm.minutesArray.push(pad(i));
      }
    }

  }

})();
