/**
 * Main function is to register a new user.
 */

(function() {
  'use strict';

  angular
    .module('register-user.controller', ['ui.router', 'ngModal'])
    .config(function($stateProvider) {
      $stateProvider
        .state('register', {
          url: '/register',
          templateUrl: 'app/views/register-user.html',
          controller: 'RegisterUserController as vm',
          data: {
            pageTitle: 'Registrar Usuario',
            background: 'cityImage'
          },
          resolve: {
            load: ['CSSInjector', function(CSSInjector) {
              return CSSInjector.set("register-user", "register-user.css");
            }]
          }
        });
    })
    .controller('RegisterUserController', RegisterUserController);

  RegisterUserController.$inject = ['UserService', '$state', '$q', 'STATUS_CODE_CREATED'];

  function RegisterUserController(UserService, $state, $q, STATUS_CODE_CREATED) {
    let vm = this;

    vm.register = function(user) {
      if (!user) {
        alert("Missing form!");
        return;
      }

      let parknerData = {};
      parknerData.first = user.firstName;
      parknerData.last = user.lastName;
      parknerData.email = user.email;
      parknerData.password = user.password;

      UserService
        .postUser(parknerData)
        .then(function(parknerResponse) {
          if (parknerResponse.status != STATUS_CODE_CREATED) {
            let data = parknerResponse.data;
            vm.error = data.error;
            return ($q.reject(new Error(vm.error)));
          }
          $state.go('successful-user-registration');
        });
    }
  }

})();
