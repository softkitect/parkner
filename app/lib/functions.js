// Close lateral menu on link click
$(document).ready(function() {
    $(".closeMenu").click(function() {
        $("#menuLateral").css("left", "-320px");
    });
    $("#hamburgerMenu").click(function() {
        $("#menuLateral").css("left", "0px");
    });
    $("#closed").click(function() {
        $("#menuLateral").css("left", "-320px");
    });
});
